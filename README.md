# Test Engineer Home Assignment

## Instructions

We have defined a very simple endpoint using OAS 3.0. 

We will be implementing the endpoint but would like to have the tests in place first. 

To help with the tests we have built a simple, functioning mock server that returns responses (see References below). 


__In no more than 2 hours effort__ put together an Acceptance Testing suite, or skeleton outline that will check the mocked responses of the /simulations endpoint that is defined in [openapi definition](openapi/openapi.yaml).

:wrench: You can create a git project of your own, or email back to HR a .zip file of your work. 

:white_check_mark: You can use your preferred tools/framework/coding language.

:boom: Ideally, triggereing the tests should be done via commmand line.

:memo: Ensure you use your initials as the Bearer Token for the 200/400 requests.

:book: Document your instructions to setup and execute the tests. 

:alarm_clock: Suite not complete after 1-2 hours? That's OK. Document some notes/plans of what your intentions are, or what's missing/TODO.

## References
There are 3 example curl requests to a mock server. 

**TIP: Ensure you have `x-mock-response-code: x` in the header, where x is `200`, `400`, or `401`**

### 200 - Get a simulated portfolio recommendations.
```
curl --location --request GET 'https://8b1afad2-2593-457b-9050-e6896a6746f5.mock.pstmn.io/simulations?riskProfileScore=0.5&financialExperienceScore=0.5&annualSavingsPercentage=0.15&totalAssets=200000&annualIncome=27500.00&age=47&incomeSource=full-time&contributionOneOffAmount=100&investmentType=gia&contributionMonthlyAmount=10000&timeHorizonYears=40&goal=grow-wealth&productId=gia-discretionary' \
--header 'x-mock-response-code: 200' \
--header 'Authorization: Bearer 1234567890'
```

### 400 - A Bad request
```
curl --location --request GET 'https://8b1afad2-2593-457b-9050-e6896a6746f5.mock.pstmn.io/simulations?riskProfileScore=0.5&financialExperienceScore=0.5&annualSavingsPercentage=0.15&totalAssets=200000&annualIncome=27500.00&age=47&incomeSource=full-time&contributionOneOffAmount=100&investmentType=gia&contributionMonthlyAmount=10000&timeHorizonYears=40&goal=grow-wealth' \
--header 'x-mock-response-code: 400' \
--header 'Authorization: Bearer 1234567890'
```

### 401 - Unauthorized
```
curl --location --request GET 'https://8b1afad2-2593-457b-9050-e6896a6746f5.mock.pstmn.io/simulations?riskProfileScore=0.5&financialExperienceScore=0.5&annualSavingsPercentage=0.15&totalAssets=200000&annualIncome=27500.00&age=47&incomeSource=full-time&contributionOneOffAmount=100&investmentType=gia&contributionMonthlyAmount=10000&timeHorizonYears=40&goal=grow-wealth&productId=gia-discretionary' \
--header 'x-mock-response-code: 401'
```
